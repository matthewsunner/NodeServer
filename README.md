# Node.jsServer

#Description
This server template is for Node.js use. It is designed with dev dependencies in place, and is a template for a MERN stack application back end. 

#Notes 
- run the install command when cloning to ensure all dependencies are pulled down. 
- update package.json as needed to match the needs of the development application. 

